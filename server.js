require('dotenv').config();
const express = require('express');
const app = express();  // se lanza el framework express

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");//Nos permite que la cabecera content type entre al backend

 next();
}

const io = require('./io');
const userController = require ('./controllers/UserController');
const authController = require ('./controllers/AuthController');
const AccountController = require ('./controllers/AccountController');

app.use(express.json()); //Intenta parsear a un json
app.use(enableCORS);

const port = process.env.PORT || 3000; //inicializas la variable, se le asigna un valor PORT si existe y en caso contrario 3000;
app.listen(port); //se pone a escuchar
console.log("API escuchando en el puerto  BIP BIP " + port);

app.get("/apitechu/v1/hello",
  function (req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API TechU"});   //respuesta de la API
  }
)

// Consultamos usuarios
app.get("/apitechu/v1/users", userController.getUsersV1);
app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);

//Creamos usuarios
app.post("/apitechu/v1/users", userController.createUserV1);
app.post("/apitechu/v2/users", userController.createUserV2);

//Creamos usuarios con id y password para Login Logout
app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v1/logout/:id", authController.logoutV1);

app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);

//Registramos la url del controlador de cuentas
app.get("/apitechu/v1/accounts/:id", AccountController.getAccountByIdV1);


//Borramos usuarios
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);
app.delete("/apitechu/v2/users/:id", userController.deleteUserV2);


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("Parametros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)
