
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuavr13ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// Lo sustituimos por el archivo .env



function getUsersV1 (req, res){
  console.log("GET /apitechu/v1/users");

  //res.sendFile('usuarios.json', {root: __dirname});

  var users = require('../usuarios.json');

  //Primero consulto si llegan los parametros
  console.log("$count" +req.query.$count);
  console.log("$top" +req.query.$top);

  //definimos un objeto vacio
  resultado = {};


  // Verificamos si viene el parámetro count y realizamos la operacion count
  if(req.query.$count =="true"){

    resultado.count=users.length;
    console.log("El tamaño del array es:" +resultado.count);

  }


  // Verificamos si viene el parámetro top y realizamos la operacion top
  if(req.query.$top != undefined){
      console.log("Viene el parámetro top" + req.query.$top);
      var total = req.query.$top;
      var elementos = users.slice(0,total);
      resultado.usuarios = elementos;

  }else{
    resultado.usuarios = users;
  }
  res.send(resultado);
}

function getUsersV2 (req, res){
  console.log("GET /apitechu/v2/users")

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" +mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body: {
        "msg" : "Error obteniendo usuarios."
      }
      res.send(response);
    }

  );
}

function getUserByIdV2 (req, res){
  console.log("GET /apitechu/v2/users/:id");

  // var id = req.params.id;
  // console.log("La id del usuario a buscar es" + id);
  // var query ='q={"id":'+ id + '}';

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query ="q=" + JSON.stringify({"id": id});
  console.log("query es" +query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo usuarios."
      }
      res.status(500);
      }else {
        if(body.length >0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }

  );

}


function createUserV1 (req, res){
  console.log("POST /apitechu/v1/users");

  //console.log(req.headers);

  //console.log(req.headers.first_name);
  //console.log(req.headers.last_name);
  //console.log(req.headers.email);

  //var newUser  = {
    //"first_name": req.headers.first_name,
    //"last_name": req.headers.last_name,


  //Lo cambiamos de headers a body

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser  = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,

  }

  console.log(newUser);

  var users = require('../usuarios.json'); //Esto es un array
  users.push(newUser); //Añadir el user al final de Users
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users); //LLamamos a la función que crea el usuario
  console.log("Proceso de creación de usuario finalizado");

}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");

  console.log(req.body.first_name);
  console.log(req.body.id);
  console.log(req.body.last_name);
  console.log(req.body.email);
  //OJO, quitar este console log
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    // No podemos guardar en claro la password:  "password": req.body.password
    "password": crypt.hash(req.body.password)
  }
  console.log(newUser);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
  function(err, resMLab, body){
    console.log("Usuario creado en Mlab");
    res.status(201).send({"msg" : "Usuario creado"});
  }

  )

}





function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id del usuario a borrar es " +req.params.id);

  var users= require("../usuarios.json");
  var posicion;

  //1. Buscamos al usuario a borrar con for

  for (var i=0; i<users.length; i++){

    console.log ("posicion i= "+i+" parameto de entrada: "+req.params.id +" +users["+i+"].id: "+users[i].id + " posicion a borrar: " + posicion);
    if(users[i].id==req.params.id) posicion=i;
    }

  //2.- Buscamos el usuario a borrar con for in

  // for (identificador in users){
  //   //console.log("Identificador: " +identificador);
  //   if(users[identificador].id==req.params.id)
  //   posicion=identificador;
  // }

  //3. for each
//   var contador=0;
//   users.forEach(function (user) {
//     if (user.id==req.params.id)
//     posicion=contador;
//     contador ++;
//   }
// )




// //4.- For of
// for (user of users){
//   if (user.id==req.params.id)
//   posicion=users.indexOf(user);
//   console.log("La posición a borrar es." +posicion);
//
// }

// //4.- For of -> Otra forma
// var contador=0;
// for (user of users){
//   if (user.id==req.params.id)
//   posicion=contador;
//   contador++;
//
// }


//  //5.Array findIndex
//  posicion=users.findIndex(function verificar(user){
//    return user.id==req.params.id;
//  }
// )
//  console.log("La posición a borrar es: " +posicion);


  users.splice(posicion, 1);
  console.log("Usuario quitado del array");
  io.writeUserDataToFile(users);

}

function deleteUserV2(req, res){
  console.log("DELETE /apitechu/v2/users/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a crear es " + id);
  var query ="q=" + JSON.stringify({"id": id});
  console.log("query es" +query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //Montamos el body a enviar
  var bodyBorrar=[{}];

  //y las cabeceras para que sean un json
  console.log(baseMLabURL+"user?" +query+"&"+mLabAPIKey);

  httpClient.put("user?" +query+"&"+mLabAPIKey,bodyBorrar,
  function (err, resMLab, body){
    console.log(resMLab);

    if(body.removed){
      res.status(200).send({"msg" : "Usuario borrado"})
    }else{
      res.status(400).send({"msg" : "Usuario no borrado porque no existe"})

      }

    }
  )

}



module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2= createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
