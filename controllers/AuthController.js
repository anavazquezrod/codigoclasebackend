const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuavr13ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1 (req, res){
  console.log("POST /apitechu/v1/loginV1");

  //Recuperamos los parámetros email y password
  var email = req.body.email;
  var password = req.body.password;

  console.log("El mail recibido es: " +email);
  console.log("La password recibida es: " +password);

  var users= require("../usuarios.json"); //Guardo en memoria los usuarios
  var i; //Contador del bucle
  var idUsuario; // Variable para guardar el Id de usuario logado

  for (i=0; i<users.length; i++){
    if((users[i].email == email)&&(users[i].password == password)){
      idUsuario=users[i].id;
      console.log("El id del usuario logado es:" +idUsuario);
      //Añadimos el atributo logged=true al usuario logado
      users[i].logged = true;
      console.log("Se actualizado el atributo logged del usuario:" +users[i].logged);
      io.writeUserDataToFile(users); //Los escribimos en fichero
      break;
    }
  }


//Mostramos el mensaje de login
var msg = idUsuario ? "Login OK":"Hay un error en el usuario o la contraseña";
console.log(msg);

//Devolvemos el resultado en un json
var resultado={};
resultado.msg= msg;
resultado.id=idUsuario;

res.send(resultado);

}

//Versión V2 de Login

function loginV2 (req, res){
  console.log("POST /apitechu/v2/loginV2");

  //Recuperamos los parámetros email y password
  var email = req.body.email;
  var password = req.body.password;

  console.log("El mail recibido es: " +email);
  console.log("La password recibida es: " +password);

  //Montar el client http para consultar las credenciales del usuarios
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  var query ="q=" + JSON.stringify({"email": email});
    console.log("query es " +query);


  //Invocamos al cliente http
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo usuarios."
      }
      res.status(500);
      res.send(response);
      }else {
        if(body.length >0){ //recuperamos email del usuario
          var response = body[0];
          //Comparamos credenciales
          console.log (body[0]);
          if(crypt.checkPassword(password,body[0].password)){
            //Actualizamos la base de datos con el parámetro LOGGED
            var bodyLogged = {"$set":{"logged":true}};
            // con el client http haciemos el put del atributo LOGGED en el cliente logado
            httpClient.put("user?" + query + "&" + mLabAPIKey, bodyLogged,
              function (err2, resMLab2, body2){
                console.log(resMLab2);

                if (!err2){
                  var response = {
                    "msg" : "Usuario logado correctamente",
                    "id" : body[0].id
                  }
                  res.status(200);
                  res.send(response);
                  }else{
                    var response = {
                      "msg" : "Internal error",

                    }
                    res.status(500);
                    res.send(response);

                  }

                }

              )

          }else{
            //Devolvemos mensaje de error de no logged
            var response = {
              "msg" : "Contraseña no vaĺida"
            }
            res.status(404);
            res.send(response);
          }

        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
        }
      }
      //res.send(response);
    }
  );
}






function logoutV1 (req, res){
  console.log("POST /apitechu/v1/logoutV1");

  var users= require("../usuarios.json"); //Guardo en memoria los usuarios

  //Recuperamos el id del usuario a deslogar
  var idUsuario = req.params.id;
  console.log("El id del usuario a deslogar es: " +idUsuario);

  var msg= "Logout incorrecto";
  var resultado={}; //Almacena el resultado del Logout

  var i;

  for(i=0; i<users.length; i++){
    //console.log(users[i]);
    if((users[i].id == idUsuario) && (users[i].logged == true)){
      //console.log("Entra en el bucle");
      //borramos la marca logged del usuario
      delete users[i].logged;
      msg = "Logout correcto";
      //Actualizamos la lista de usuarios
      io.writeUserDataToFile(users);
      resultado.id = idUsuario;
      break;
    }

  }

  resultado.msg=msg;

  res.send(resultado);
}




function logoutV2 (req, res){
  console.log("POST /apitechu/v2/logoutV2");

  //Recuperamos el id del usuario a deslogar

  var idUsuario = Number.parseInt(req.params.id);
  console.log("El id del usuario a deslogar es: " +idUsuario);

  //Crear el client http para consultar el usuario a deslogar
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  var query ="q=" + JSON.stringify({"id": idUsuario});
  console.log("query es " +query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo usuarios."
      }
      res.status(500);
      res.send(response);
      }else {
        if(body.length >0){
          var response = body[0];
          //Verificamos si usuario esta logado
          if (body[0].logged==true){
            //Actualizamos la base de datos quitando el parámetro LOGGED
            var bodyLogged = {"$unset":{"logged":""}};
            // con el client http haciemos el borrado del atributo LOGGED en el cliente logado
            httpClient.put("user?" + query + "&" + mLabAPIKey, bodyLogged,
              function (err2, resMLab2, body2){
                console.log(resMLab2);

                if (!err2){
                  var response = {
                    "msg" : "Usuario deslogado correctamente"
                  }
                  res.status(200);
                  res.send(response);
                  }else{
                    var response = {
                      "msg" : "Internal error"
                    }
                    res.status(500);
                    res.send(response);

                  }

                }

              )


          }else{
            var response = {
              "msg" : "Usuario no logado"
            }
            res.status(403);
            res.send(response)
          }
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
        }
      }
      //res.send(response);
    }

  );



}





module.exports.loginV1=loginV1;
module.exports.logoutV1=logoutV1;

module.exports.loginV2=loginV2;
module.exports.logoutV2=logoutV2;
