const mocha =require('mocha');   // cargamos libreria mocha
const chai =require('chai');   // cargamos libreria chai
const chaihttp =require('chai-http');   // cargamos libreria chai-http

chai.use(chaihttp); //Usamos la variable, que contiene la libreria

var should = chai.should();

describe("First test",  //describe para agrupar test unitarios
  function(){
    it('Test that Duckduckgo works', function (done){
      chai.request('http://www.duckduckgo.com')
      .get('/')
      .end(
        function (err, res){
          console.log("Request finished");
          //console.log(res);
          console.log(err);

          res.should.have.status(200);
          done(); //En un contexto asíncrono para saber cuándo tiene que hacer la aserción
          }
        )
      }
    )
  }
)


describe("Test de API Usuarios",  //describe para agrupar test unitarios
  function(){
    it('TPrueba que la API de Usuarios responde', function (done){
      chai.request('http://localhost:3000')
      .get('/apitechu/v1/hello')
      .end(
        function (err, res){
          console.log("Request finished");

          res.should.have.status(200);
          res.body.msg.should.be.equal("Hola desde API TechU");
          done(); //En un contexto asíncrono para saber cuándo tiene que hacer la aserción
          }
        )
      }
    ), it('TPrueba que la API devuelve una lista de Usuarios correctos', function (done){
      chai.request('http://localhost:3000')
      .get('/apitechu/v1/users')
      .end(
        function (err, res){
          console.log("Request finished");

          res.should.have.status(200); //AP I responde
          res.body.usuarios.should.be.a("array");
          
          for(user of res.body.usuarios){
            user.should.have.property("email");
            user.should.have.property("first_name");
          }

          done(); //En un contexto asíncrono para saber cuándo tiene que hacer la aserción
          }
        )
      }
    )


  }
)
