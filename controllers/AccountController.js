
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuavr13ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// Lo sustituimos por el archivo .env


//función que recupera las cuentas del usuario pasándole el id de usuario logado
function getAccountByIdV1 (req, res){
  console.log("GET /apitechu/v1/accounts/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query ="q=" + JSON.stringify({"userid": id});
  console.log("query es" +query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if(err){
      var response = {
        "msg" : "Error obteniendo cuentas."
      }
      res.status(500);
      }else {
        if(body.length >0){
          var response = body;
        }else{
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}



module.exports.getAccountByIdV1 = getAccountByIdV1;
