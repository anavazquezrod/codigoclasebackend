//Función que escribe el usuario en un fichero


const fs = require('fs'); //fs es un módulo que te permite escribir en el disco


function writeUserDataToFile(data){
  console.log("writeUserDataToFile");

  //Ahora escribimos el nuevo usuario

  var jsonUserData = JSON.stringify(data); //lo convertimos a json

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
      console.log(err);
    }else{
      console.log("USuario persistido en fichero de usuarios");
    }
  }
 )
}


//Exportas la función para que pueda utilizarse fuera llamándole desde otro punto del código
module.exports.writeUserDataToFile= writeUserDataToFile;
