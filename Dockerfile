# Imagen raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de proyecto
ADD . /apitechu

# Instalo paquetes necesarios
RUN npm install --only-prod

# Abrimos el puerto que vamos a utilizar o puerto que expone
EXPOSE 3000

# Comando de inicialización
CMD ["node", "server.js"]
